use std::io::Read;
use std::io::{self, Write};
use futures::{Future, Stream};
use hyper::Client;
use tokio_core::reactor::Core;
use serde::ser::{Serialize,Serializer,SerializeStruct};
use serde_json;


use iron::prelude::*;
use iron::status;
#[derive(Deserialize,Debug,Serialize)]
pub struct PushHook {
    user_name: String,
    object_kind: String,
    repository: Repository,

}
#[derive(Deserialize,Debug,Serialize)]
pub struct Repository {
    name: String,
    url: String,
    description: String,
    homepage: String,
    git_http_url: String,
    git_ssh_url: String,
    visibility_level: u8,
}








pub fn echo(request: &mut Request) -> PushHook{
    let mut body = Vec::new();
    request
        .body
        .read_to_end(&mut body)
        .map_err(|e| IronError::new(e,(status::InternalServerError,"Error reading request"))).unwrap();
    let message = String::from_utf8(body).expect("Found invalid UTF8");
    println!("{}",message);
    let deserialize: PushHook = serde_json::from_str(&message).unwrap();
    println!("Repo:{}",deserialize.repository.name);
    deserialize
}






