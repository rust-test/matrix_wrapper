extern crate matrix_wrapper;
extern crate iron;
extern crate router;

use iron::prelude::*;
use iron::status;
use router::Router;
use matrix_wrapper::gitlab_wrapper::echo;
use matrix_wrapper::gitlab_wrapper::{Repository,PushHook};
use matrix_wrapper::matrix_wrapper::{MatrixServer,Event,RoomMessageType,MessageBody};



static TOKEN: &'static str = "MDAxYmxvY2F0aW9uIG1hdHJpeC5jYmIuZGUKMDAxM2lkZW50aWZpZXIga2V5CjAwMTBjaWQgZ2VuID0gMQowMDI4Y2lkIHVzZXJfaWQgPSBAbWFzdmVuOm1hdHJpeC5jYmIuZGUKMDAxNmNpZCB0eXBlID0gYWNjZXNzCjAwMjFjaWQgbm9uY2UgPSBCMUlzdzgucXFeVmc6QFM7CjAwMmZzaWduYXR1cmUgmEIOpQ690kz-7MAlBad152beXfwL4lWn3MnO7pYMC98K";

fn main() {
    let mut router = Router::new();
    router.post("/", handler,"index");
    Iron::new(router).http("localhost:3000").unwrap();

    let matrix_server = MatrixServer::new("https://matrix.cbb.de",TOKEN,443,"!lYgmuQWkMUYZgaiseK:matrix.cbb.de");

    fn handler(req: &mut Request) -> IronResult<Response>{
        let ref query = req.extensions.get::<Router>().unwrap().find("query").unwrap_or("/");
        Ok(Response::with((status::Ok,*query))
    }

}

