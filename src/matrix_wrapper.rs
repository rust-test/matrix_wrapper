use serde::ser::{Serialize, SerializeStruct, Serializer};
use serde_json;
use std::io::{self, Write};
use futures::{Future, Stream};
use hyper::Client;
use tokio_core::reactor::Core;
use hyper::{Method, Request};
use hyper::header::{ContentLength, ContentType};


//Typ der Nachricht
#[derive(Debug, Deserialize)]
pub enum RoomMessageType {
    MIMAGE,
    MTEXT,
    MAUDIO,
    MVIDEO,
}

impl Serialize for RoomMessageType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        match *self {
            RoomMessageType::MTEXT => serializer.serialize_unit_variant("msgtype", 1, "m.text"),
            RoomMessageType::MIMAGE => serializer.serialize_unit_variant("msgtype", 0, "m.image"),
            RoomMessageType::MAUDIO => serializer.serialize_unit_variant("msgtype", 2, "m.audio"),
            RoomMessageType::MVIDEO => serializer.serialize_unit_variant("msgtype", 3, "m.video"),
        }
    }
}

#[derive(Deserialize, Debug)]
pub enum EventType {
    MRoomMessage,
}

impl PartialEq for EventType {
    fn eq(&self, other: &EventType) -> bool {
        self == other
    }
}

pub struct Event {
    //Alter der Nachricht seit dem die Nachricht gesendet wurde
    age: u64,
    content: MessageBody,
    event_id: String,
    origin_server_ts: u64,
    room_id: String,
    sender: String,
    event_type: EventType,
}

pub struct MatrixServer{
    url: String,
    token: String,
    port: u16,
    room: String,
}
static ROOM_PATH: &'static str = "_matrix/client/r0/rooms/";
static MESSAGE_PATH: &'static str = "send/m.room.message";
static MESSAGE_ID: u64 = 12000;
static QUESTION_MARK: &'static str = "?";
static ACCESS_TOKEN: &'static str = "access_token=";


impl MatrixServer{
    pub fn send(&self,matrix_event: Event) {
        let  body = serde_json::to_string(&matrix_event.content).unwrap().to_owned();
        let mut uri = self.url.to_owned();
        uri.push_str(ROOM_PATH);
        uri.push_str(&self.room);
        uri.push_str(MESSAGE_PATH);
        uri.push_str(&matrix_event.age.to_string());
        uri.push_str(QUESTION_MARK);
        uri.push_str(ACCESS_TOKEN);
        uri.push_str(&self.token);
        let mut core = Core::new().unwrap();
        let client = Client::new(&core.handle());
        let mut req = Request::new(Method::Post,uri.parse().unwrap());
        req.headers_mut().set(ContentType::json());
        req.headers_mut().set(ContentLength(body.len()as u64)) ;
        req.set_body(body);
        client.request(req).and_then(|res| {
            println!("POST: {}", res.status());

            res.body().concat2()
        });
    }
}


#[derive(Debug, Serialize)]
pub struct MessageBody {
    body: String,
    msgtype: RoomMessageType,
}

impl Serialize for EventType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        match *self {
            EventType::MRoomMessage => {
                serializer.serialize_unit_variant("type", 0, "m.room.message")
            }
        }
    }
}

impl Serialize for Event {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        let mut state = serializer.serialize_struct("Event", 7)?;
        state.serialize_field("age", &self.age)?;
        state.serialize_field("content", &self.content)?;
        state.serialize_field("event_id", &self.event_id)?;
        state.serialize_field("origin_server_ts", &self.origin_server_ts)?;
        state.serialize_field("room_id", &self.room_id)?;
        state.serialize_field("sender", &self.sender)?;
        state.serialize_field("type", &self.event_type)?;
        state.end()
    }
}

#[cfg(test)]
mod tests {
    extern crate serde_test;
    use self::serde_test::{assert_ser_tokens, Token};
    use super::*;
    #[test]
    fn test_event_type() {
        let event = EventType::MRoomMessage;
        assert_ser_tokens(
            &event,
            &[
                Token::UnitVariant {
                    name: "type",
                    variant: "m.room.message",
                },
            ],
        )
    }

    #[test]
    fn test_event_type_ser_string_repr() {
        let event = EventType::MRoomMessage;
        let j = serde_json::to_string(&event).unwrap();
        assert_eq!(j, "\"m.room.message\"");
    }
    #[test]
    fn test_event() {
        let event = Event {
            event_type: EventType::MRoomMessage,
            sender: String::from("blah"),
            room_id: String::from("blah"),
            origin_server_ts: 1234,
            event_id: String::from("blah"),
            content: MessageBody {
                body: String::from("blah"),
                msgtype: RoomMessageType::MTEXT,
            },
            age: 12,
        };
        let j = serde_json::to_string(&event).unwrap();
        println!("{}", j);
        assert_ser_tokens(
            &event,
            &[
                Token::Struct {
                    name: "Event",
                    len: 7,
                },
                Token::Str("age"),
                Token::U64(12),
                Token::Str("content"),
                Token::Struct {
                    name: "MessageBody",
                    len: 2,
                },
                Token::Str("body"),
                Token::Str("blah"),
                Token::Str("msgtype"),
                Token::UnitVariant {
                    name: "msgtype",
                    variant: "m.text",
                },
                Token::StructEnd,
                Token::Str("event_id"),
                Token::Str("blah"),
                Token::Str("origin_server_ts"),
                Token::U64(1234),
                Token::Str("room_id"),
                Token::Str("blah"),
                Token::Str("sender"),
                Token::Str("blah"),
                Token::Str("type"),
                Token::UnitVariant {
                    name: "type",
                    variant: "m.room.message",
                },
                Token::StructEnd,
            ],
        );
    }
}